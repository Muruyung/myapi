package console

import (
	"MyAPI/config"
	"MyAPI/delivery/http/merchant"
	"MyAPI/delivery/http/outlet"
	"MyAPI/delivery/http/transaction"
	"MyAPI/delivery/http/user"
)

func Run() {
	app := config.New()
	db := config.DBase()
	user.Init(app, db)
	merchant.Init(app, db)
	outlet.Init(app, db)
	transaction.Init(app, db)
	config.Listen(app)
}
