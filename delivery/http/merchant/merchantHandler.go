package merchant

import (
	"MyAPI/config"
	"MyAPI/controller/merchant"

	"github.com/kataras/iris/v12"
	"xorm.io/xorm"
)

func Init(app *iris.Application, db *xorm.Engine) {
	userAPI := config.Party(app, "/merchant")
	{
		userAPI.Use(iris.Compression)
		userAPI.Post("/", merchant.Init(db).Create)
		userAPI.Get("/", merchant.Init(db).ReadList)
		userAPI.Get("/{id}", merchant.Init(db).ReadById)
		userAPI.Put("/", merchant.Init(db).Update)
		userAPI.Delete("/{id}", merchant.Init(db).Delete)
	}
}
