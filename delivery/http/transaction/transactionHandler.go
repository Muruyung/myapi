package transaction

import (
	"MyAPI/config"
	"MyAPI/controller/transaction"

	"github.com/kataras/iris/v12"
	"xorm.io/xorm"
)

func Init(app *iris.Application, db *xorm.Engine) {
	userAPI := config.Party(app, "/transaction")
	{
		userAPI.Use(iris.Compression)
		userAPI.Post("/", transaction.Init(db).Create)
		userAPI.Get("/", transaction.Init(db).ReadList)
		userAPI.Get("/{id}", transaction.Init(db).ReadById)
		userAPI.Put("/", transaction.Init(db).Update)
		userAPI.Delete("/{id}", transaction.Init(db).Delete)
	}
}
