package outlet

import (
	"MyAPI/config"
	"MyAPI/controller/outlet"

	"github.com/kataras/iris/v12"
	"xorm.io/xorm"
)

func Init(app *iris.Application, db *xorm.Engine) {
	userAPI := config.Party(app, "/outlet")
	{
		userAPI.Use(iris.Compression)
		userAPI.Post("/", outlet.Init(db).Create)
		userAPI.Get("/", outlet.Init(db).ReadList)
		userAPI.Get("/{id}", outlet.Init(db).ReadById)
		userAPI.Put("/", outlet.Init(db).Update)
		userAPI.Delete("/{id}", outlet.Init(db).Delete)
	}
}
