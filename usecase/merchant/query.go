package merchant

import (
	"MyAPI/entity"
	"fmt"

	_ "github.com/lib/pq"
	"xorm.io/xorm"
)

// GetList query for select all data
func GetList(db *xorm.Engine, userId int64) (merchant []entity.Merchants, err error) {
	err = db.Where(fmt.Sprintf("user_id=%d", userId)).Find(&merchant)
	return
}

// GetById query for select data by ID
func GetById(db *xorm.Engine, id int64, userId int64) (merchant entity.Merchants, err error) {
	_, err = db.Where(fmt.Sprintf("id=%d AND user_id=%d", id, userId)).Get(&merchant)
	return
}

// Post query for insert data
func Post(db *xorm.Engine, merchant entity.Merchants) (err error) {
	_, err = db.Insert(
		entity.Merchants{
			Id:           merchant.Id,
			UserId:       merchant.UserId,
			MerchantName: merchant.MerchantName,
			CreatedBy:    merchant.CreatedBy,
			UpdatedBy:    merchant.UpdatedBy,
		},
	)
	return
}

// Put query for update data
func Put(db *xorm.Engine, merchant entity.Merchants) (err error) {
	_, err = db.Where(fmt.Sprintf("id=%d", merchant.Id)).Update(
		entity.Merchants{
			UserId:       merchant.UserId,
			MerchantName: merchant.MerchantName,
			UpdatedBy:    merchant.UpdatedBy,
		},
	)
	return
}

// Delete query for delete data
func Delete(db *xorm.Engine, id int64) (err error) {
	var merchant entity.Merchants
	_, err = db.Where(fmt.Sprintf("id=%d", id)).Delete(&merchant)
	return
}
