package outlet

import (
	"MyAPI/entity"
	"fmt"

	_ "github.com/lib/pq"
	"xorm.io/xorm"
)

// GetList query for select all data
func GetList(db *xorm.Engine) (outlet []entity.Outlets, err error) {
	err = db.Find(&outlet)
	return
}

// GetById query for select data by ID
func GetById(db *xorm.Engine, id int64) (outlet entity.Outlets, err error) {
	_, err = db.Where(fmt.Sprintf("id=%d", id)).Get(&outlet)
	return
}

// Post query for insert data
func Post(db *xorm.Engine, outlet entity.Outlets) (err error) {
	_, err = db.Insert(
		entity.Outlets{
			Id:         outlet.Id,
			MerchantId: outlet.MerchantId,
			OutletName: outlet.OutletName,
			CreatedBy:  outlet.CreatedBy,
			UpdatedBy:  outlet.UpdatedBy,
		},
	)
	return
}

// Put query for update data
func Put(db *xorm.Engine, outlet entity.Outlets) (err error) {
	_, err = db.Where(fmt.Sprintf("id=%d", outlet.Id)).Update(
		entity.Outlets{
			MerchantId: outlet.MerchantId,
			OutletName: outlet.OutletName,
			UpdatedBy:  outlet.UpdatedBy,
		},
	)
	return
}

// Delete query for delete data
func Delete(db *xorm.Engine, id int64) (err error) {
	var outlet entity.Outlets
	_, err = db.Where(fmt.Sprintf("id=%d", id)).Delete(&outlet)
	return
}
