package transaction

import (
	"MyAPI/entity"
	"fmt"

	_ "github.com/lib/pq"
	"xorm.io/xorm"
)

// GetList query for select all data
func GetList(db *xorm.Engine) (transaction []entity.Transactions, err error) {
	err = db.Find(&transaction)
	return
}

// GetById query for select data by ID
func GetById(db *xorm.Engine, id int64) (transaction entity.Transactions, err error) {
	_, err = db.Where(fmt.Sprintf("id=%d", id)).Get(&transaction)
	return
}

// Post query for insert data
func Post(db *xorm.Engine, transaction entity.Transactions) (err error) {
	_, err = db.Insert(
		entity.Transactions{
			Id:         transaction.Id,
			MerchantId: transaction.MerchantId,
			OutletId:   transaction.OutletId,
			BillTotal:  transaction.BillTotal,
			CreatedBy:  transaction.CreatedBy,
			UpdatedBy:  transaction.UpdatedBy,
		},
	)
	return
}

// Put query for update data
func Put(db *xorm.Engine, transaction entity.Transactions) (err error) {
	_, err = db.Where(fmt.Sprintf("id=%d", transaction.Id)).Update(
		entity.Transactions{
			MerchantId: transaction.MerchantId,
			OutletId:   transaction.OutletId,
			BillTotal:  transaction.BillTotal,
			UpdatedBy:  transaction.UpdatedBy,
		},
	)
	return
}

// Delete query for delete data
func Delete(db *xorm.Engine, id int64) (err error) {
	var transaction entity.Transactions
	_, err = db.Where(fmt.Sprintf("id=%d", id)).Delete(&transaction)
	return
}
