package user

import (
	"MyAPI/config"
	"MyAPI/delivery/helper"
	"MyAPI/entity"
	"fmt"

	_ "github.com/lib/pq"
	"xorm.io/xorm"
)

// GetList query for select all data
func GetList(db *xorm.Engine) (user []entity.Users, err error) {
	err = db.Find(&user)
	return
}

// GetById query for select data by ID
func GetById(db *xorm.Engine, id int64) (user entity.Users, err error) {
	_, err = db.Where(fmt.Sprintf("id=%d", id)).Get(&user)
	return
}

// GetByUserName query for select data by user name
func GetByUserName(db *xorm.Engine, username string) (user entity.Users, err error) {
	_, err = db.Where(fmt.Sprintf("user_name='%s'", username)).Get(&user)
	return
}

// GetByToken query for select data by token
func GetByToken(db *xorm.Engine, token string) (user entity.Users, err error) {
	var tb_token entity.Tokens
	_, err = db.Where(fmt.Sprintf("token='%s'", token)).Get(&tb_token)
	if err != nil {
		return
	}

	_, err = db.Where(fmt.Sprintf("id=%d", tb_token.Id_user)).Get(&user)
	return
}

// GetToken query for generate token
func GetToken(db *xorm.Engine, logBody entity.LoginBody) (token entity.LoginResponse, err error) {
	var user entity.Users
	_, err = db.Where(fmt.Sprintf("user_name='%s'", logBody.Username)).Get(&user)
	if err != nil {
		return
	}

	err = user.CheckPassword(logBody.Password)
	if err != nil {
		return
	}

	jwtWrapper := helper.JwtWrapper{
		SecretKey:       config.JWTSECRETKEY(),
		Issuer:          "AuthService",
		ExpirationHours: int64(config.JWTEXPIRATIONHOUR()),
	}

	signedToken, err := jwtWrapper.GenerateToken(user.UserName)
	if err != nil {
		return
	}

	token = entity.LoginResponse{
		Token: signedToken,
	}

	err = SetToken(db, entity.Tokens{
		Id_user: user.Id,
		Tokens:  token.Token,
	})
	if err != nil {
		return
	}

	return
}

// PostToken query for insert (initial) token
func PostToken(db *xorm.Engine, token entity.Tokens) (err error) {
	_, err = db.Insert(
		entity.Tokens{
			Id:      token.Id,
			Id_user: token.Id_user,
			Tokens:  "",
		},
	)
	return
}

// SetToken query for update token
func SetToken(db *xorm.Engine, token entity.Tokens) (err error) {
	_, err = db.Where(fmt.Sprintf("id_user=%d", token.Id_user)).Update(
		entity.Tokens{
			Tokens: token.Tokens,
		},
	)
	return
}

// DeleteToken query for delete token data
func DeleteToken(db *xorm.Engine, id int64) (err error) {
	var token entity.Tokens
	_, err = db.Where(fmt.Sprintf("id_user=%d", id)).Delete(&token)
	return
}

// Post query for insert data
func Post(db *xorm.Engine, user entity.Users) (err error) {
	_, err = db.Insert(
		entity.Users{
			Id:        user.Id,
			Name:      user.Name,
			Password:  user.Password,
			UserName:  user.UserName,
			CreatedBy: user.CreatedBy,
			UpdatedBy: user.UpdatedBy,
		},
	)
	return
}

// Put query for update data
func Put(db *xorm.Engine, user entity.Users) (err error) {
	_, err = db.Where(fmt.Sprintf("id=%d", user.Id)).Update(
		entity.Users{
			Name:      user.Name,
			Password:  user.Password,
			UserName:  user.UserName,
			CreatedBy: user.CreatedBy,
			UpdatedBy: user.UpdatedBy,
		},
	)
	return
}

// Delete query for delete data
func Delete(db *xorm.Engine, id int64) (err error) {
	var user entity.Users
	err = DeleteToken(db, id)
	if err != nil {
		return
	}
	_, err = db.Where(fmt.Sprintf("id=%d", id)).Delete(&user)
	return
}
