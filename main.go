package main

import (
	"MyAPI/delivery/console"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	console.Run()
}
