# MyAPI

## Description
Simple API using clean code architecture

This project has  4 Domain layer :
 * Entity Layer
 * Usecase Layer
 * Controller Layer  
 * Delivery Layer

## Tools Used:
 * [PostgreSQL](https://github.com/lib/pq)
 * [Iris Lib](https://github.com/kataras/iris)
 * [Xorm Lib](https://xorm.io/xorm)
 * [JWT](https://github.com/dgrijalva/jwt-go)
 * [bcrypt](https://golang.org/x/crypto/bcrypt)

## Database
### 📄 [Database File](https://github.com/Muruyung/MyAPI/tree/master/SQL)

## Documentation API
### ✉ [Postman Documentation API](https://documenter.getpostman.com/view/12251646/UzQuNQUu)
