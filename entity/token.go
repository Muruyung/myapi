package entity

type Tokens struct {
	Id      int64  `xorm:"not null 'id'" json:"id"`
	Id_user int64  `xorm:"not null 'id_user'" json:"id_user"`
	Tokens  string `xorm:"not null 'token'" json:"token"`
}
