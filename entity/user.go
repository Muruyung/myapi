package entity

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

type Users struct {
	Id        int64     `xorm:"not null 'id'" json:"id"`
	Name      string    `xorm:"name" json:"name"`
	UserName  string    `xorm:"user_name" json:"user_name"`
	Password  string    `xorm:"password" json:"password"`
	CreatedAt time.Time `xorm:"created" json:"created_at"`
	CreatedBy int64     `xorm:"not null 'created_by'" json:"created_by"`
	UpdatedAt time.Time `xorm:"updated" json:"updated_at"`
	UpdatedBy int64     `xorm:"not null 'updated_by'" json:"updated_by"`
}

// NewUser create new user
func NewUser(username string, name string, password string) (user Users) {
	id := time.Now().UnixNano() / int64(time.Millisecond)

	user = Users{
		Id:        id,
		Name:      name,
		UserName:  username,
		Password:  password,
		CreatedBy: id,
		UpdatedBy: id,
	}

	return
}

// HashPassword encrypts user password
func (user *Users) HashPassword(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return err
	}

	user.Password = string(bytes)

	return nil
}

// CheckPassword checks user password
func (user *Users) CheckPassword(providedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(providedPassword))
	if err != nil {
		return err
	}

	return nil
}
