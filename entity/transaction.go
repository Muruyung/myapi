package entity

import "time"

type Transactions struct {
	Id         int64     `xorm:"id" json:"id"`
	MerchantId int64     `xorm:"merchant_id" json:"merchant_id"`
	OutletId   int64     `xorm:"outlet_id" json:"outlet_id"`
	BillTotal  float64   `xorm:"bill_total" json:"bill_total"`
	CreatedAt  time.Time `xorm:"created" json:"created_at"`
	CreatedBy  int64     `xorm:"created_by" json:"created_by"`
	UpdatedAt  time.Time `xorm:"updated" json:"updated_at"`
	UpdatedBy  int64     `xorm:"updated_by" json:"updated_by"`
}
