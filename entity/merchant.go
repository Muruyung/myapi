package entity

import "time"

type Merchants struct {
	Id           int64     `xorm:"id" json:"id"`
	UserId       int64     `xorm:"user_id" json:"user_id"`
	MerchantName string    `xorm:"merchant_name" json:"merchant_name"`
	CreatedAt    time.Time `xorm:"created" json:"created_at"`
	CreatedBy    int64     `xorm:"created_by" json:"created_by"`
	UpdatedAt    time.Time `xorm:"updated" json:"updated_at"`
	UpdatedBy    int64     `xorm:"updated_by" json:"updated_by"`
}

// NewMerchant create new merchant
func NewMerchant(merchant Merchants, userId int64) Merchants {
	id := time.Now().UnixNano() / int64(time.Millisecond)

	merchant = Merchants{
		Id:           id,
		UserId:       userId,
		MerchantName: merchant.MerchantName,
		CreatedBy:    userId,
		UpdatedBy:    userId,
	}

	return merchant
}
