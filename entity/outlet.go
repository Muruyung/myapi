package entity

import "time"

type Outlets struct {
	Id         int64     `json:"id" xorm:"id"`
	MerchantId int64     `json:"merchant_id" xorm:"merchant_id"`
	OutletName string    `json:"outlet_name" xorm:"outlet_name"`
	CreatedAt  time.Time `json:"created_at" xorm:"created"`
	CreatedBy  int64     `json:"created_by" xorm:"created_by"`
	UpdatedAt  time.Time `json:"updated_at" xorm:"updated"`
	UpdatedBy  int64     `json:"updated_by" xorm:"updated_by"`
}

// NewOutlet create new outlet
func NewOutlet(outlet Outlets, userId int64) Outlets {
	id := time.Now().UnixNano() / int64(time.Millisecond)

	outlet = Outlets{
		Id:         id,
		MerchantId: outlet.MerchantId,
		OutletName: outlet.OutletName,
		CreatedBy:  userId,
		UpdatedBy:  userId,
	}

	return outlet
}
