package user

import "xorm.io/xorm"

type Database struct {
	engine *xorm.Engine
}

// Init init database for user
func Init(DB *xorm.Engine) Database {
	return Database{engine: DB}
}
