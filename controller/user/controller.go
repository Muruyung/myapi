package user

import (
	"MyAPI/controller"
	"MyAPI/delivery/helper"
	"MyAPI/entity"
	"MyAPI/usecase/user"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/kataras/iris/v12"
)

// Login controller using post method
func (db Database) Login(ctx iris.Context) {
	var logBody entity.LoginBody

	err := ctx.ReadJSON(&logBody)
	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	token, err := user.GetToken(db.engine, logBody)
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(token).JSON()
}

// Logout controller using post method
func (db Database) Logout(ctx iris.Context) {
	var logResponse entity.LoginResponse
	err := ctx.ReadJSON(&logResponse)

	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	tb_user, err := user.GetByToken(db.engine, logResponse.Token)
	if err != nil {
		ctx.StopWithStatus(iris.StatusNotFound)
		helper.CreateErrorResponse(ctx, err).NotFound().JSON()
		return
	}

	err = user.SetToken(db.engine, entity.Tokens{
		Id_user: tb_user.Id,
		Tokens:  "-",
	})
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(tb_user).JSON()
}

// Create controller using post method
func (db Database) Create(ctx iris.Context) {
	var tb_user entity.Users

	err := ctx.ReadJSON(&tb_user)
	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	tmpUser, _ := user.GetByUserName(db.engine, tb_user.UserName)
	if tmpUser.UserName != "" {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, errors.New("username already exist")).BadRequest().JSON()
		return
	}

	err = tb_user.HashPassword(tb_user.Password)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	tb_user = entity.NewUser(tb_user.UserName, tb_user.Name, tb_user.Password)
	err = user.Post(db.engine, tb_user)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	var token entity.Tokens
	token.Id = time.Now().UnixNano() / int64(time.Millisecond)
	token.Id_user = tb_user.Id
	err = user.PostToken(db.engine, token)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(tb_user).JSON()
}

// ReadList controller using get method
func (db Database) ReadList(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	tb_user, err := user.GetList(db.engine)

	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(tb_user).JSON()
}

// ReadById controller using get method
func (db Database) ReadById(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	id, err := strconv.ParseInt(ctx.Params().Get("id"), 10, 64)
	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	tb_user, err := user.GetById(db.engine, id)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(tb_user).JSON()
}

// Update controller using put method
func (db Database) Update(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	var tb_user entity.Users
	err = ctx.ReadJSON(&tb_user)

	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	err = tb_user.HashPassword(tb_user.Password)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	err = user.Put(db.engine, tb_user)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(tb_user).JSON()
}

// Delete controller using delete method
func (db Database) Delete(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	id, err := strconv.ParseInt(ctx.Params().Get("id"), 10, 64)

	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	err = user.Delete(db.engine, id)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetMessage(fmt.Sprintf("%d deleted", id)).JSON()
}
