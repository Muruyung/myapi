package controller

import (
	"MyAPI/config"
	"MyAPI/delivery/helper"
	"MyAPI/entity"
	"MyAPI/usecase/user"
	"errors"
	"strings"

	"xorm.io/xorm"
)

// BearerTokenAuth nodoc
func BearerTokenAuth(engine *xorm.Engine, bearerToken string) (entity.Users, error) {
	bearer := strings.Split(bearerToken, " ")
	token := bearer[1]
	tb_user, err := user.GetByToken(engine, token)
	if err != nil || tb_user.UserName == "" {
		return entity.Users{}, errors.New("unauthorized")
	}

	jwtWrapper := helper.JwtWrapper{
		SecretKey:       config.JWTSECRETKEY(),
		Issuer:          "AuthService",
		ExpirationHours: int64(config.JWTEXPIRATIONHOUR()),
	}

	_, err = jwtWrapper.ValidateToken(token)
	if err != nil {
		return entity.Users{}, err
	}

	return tb_user, nil
}
