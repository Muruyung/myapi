package transaction

import (
	"MyAPI/controller"
	"MyAPI/delivery/helper"
	"MyAPI/entity"
	"MyAPI/usecase/transaction"
	"fmt"
	"strconv"
	"time"

	"github.com/kataras/iris/v12"
)

// Create controller using post method
func (db Database) Create(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	var transactions entity.Transactions

	err = ctx.ReadJSON(&transactions)
	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	transactions.Id = time.Now().UnixNano() / int64(time.Millisecond)
	err = transaction.Post(db.engine, transactions)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(transactions).JSON()
}

// ReadList controller using get method
func (db Database) ReadList(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	transactions, err := transaction.GetList(db.engine)

	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(transactions).JSON()
}

// ReadById controller using get method
func (db Database) ReadById(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	id, err := strconv.ParseInt(ctx.Params().Get("id"), 10, 64)
	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	transactions, err := transaction.GetById(db.engine, id)

	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(transactions).JSON()
}

// Update controller using put method
func (db Database) Update(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	var transactions entity.Transactions
	err = ctx.ReadJSON(&transactions)

	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	err = transaction.Put(db.engine, transactions)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(transactions).JSON()
}

// Delete controller using delete method
func (db Database) Delete(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	id, err := strconv.ParseInt(ctx.Params().Get("id"), 10, 64)

	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	err = transaction.Delete(db.engine, id)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetMessage(fmt.Sprintf("%d deleted", id)).JSON()
}
