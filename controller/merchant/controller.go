package merchant

import (
	"MyAPI/controller"
	"MyAPI/delivery/helper"
	"MyAPI/entity"
	"MyAPI/usecase/merchant"
	"fmt"
	"strconv"

	"github.com/kataras/iris/v12"
)

// Create controller using post method
func (db Database) Create(ctx iris.Context) {
	user, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	var merchants entity.Merchants

	err = ctx.ReadJSON(&merchants)
	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	merchants = entity.NewMerchant(merchants, user.Id)
	err = merchant.Post(db.engine, merchants)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(merchants).JSON()
}

// ReadList controller using get method
func (db Database) ReadList(ctx iris.Context) {
	user, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	merchants, err := merchant.GetList(db.engine, user.Id)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(merchants).JSON()
}

// ReadById controller using get method
func (db Database) ReadById(ctx iris.Context) {
	user, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	id, err := strconv.ParseInt(ctx.Params().Get("id"), 10, 64)
	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	merchants, err := merchant.GetById(db.engine, id, user.Id)

	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(merchants).JSON()
}

// Update controller using put method
func (db Database) Update(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	var merchants entity.Merchants
	err = ctx.ReadJSON(&merchants)

	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	err = merchant.Put(db.engine, merchants)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(merchants).JSON()
}

// Delete controller using delete method
func (db Database) Delete(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	id, err := strconv.ParseInt(ctx.Params().Get("id"), 10, 64)

	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	err = merchant.Delete(db.engine, id)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetMessage(fmt.Sprintf("%d deleted", id)).JSON()
}
