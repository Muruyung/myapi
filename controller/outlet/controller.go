package outlet

import (
	"MyAPI/controller"
	"MyAPI/delivery/helper"
	"MyAPI/entity"
	"MyAPI/usecase/outlet"
	"fmt"
	"strconv"

	"github.com/kataras/iris/v12"
)

// Create controller using post method
func (db Database) Create(ctx iris.Context) {
	user, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	var outlets entity.Outlets

	err = ctx.ReadJSON(&outlets)
	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	outlets = entity.NewOutlet(outlets, user.Id)
	err = outlet.Post(db.engine, outlets)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(outlets).JSON()
}

// ReadList controller using get method
func (db Database) ReadList(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	outlets, err := outlet.GetList(db.engine)

	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(outlets).JSON()
}

// ReadById controller using get method
func (db Database) ReadById(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	id, err := strconv.ParseInt(ctx.Params().Get("id"), 10, 64)
	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	outlets, err := outlet.GetById(db.engine, id)

	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(outlets).JSON()
}

// Update controller using put method
func (db Database) Update(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	var outlets entity.Outlets
	err = ctx.ReadJSON(&outlets)

	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	err = outlet.Put(db.engine, outlets)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetData(outlets).JSON()
}

// Delete controller using delete method
func (db Database) Delete(ctx iris.Context) {
	_, err := controller.BearerTokenAuth(db.engine, ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.StopWithStatus(iris.StatusUnauthorized)
		helper.CreateErrorResponse(ctx, err).Unauthorized().JSON()
		return
	}

	id, err := strconv.ParseInt(ctx.Params().Get("id"), 10, 64)

	if err != nil {
		ctx.StopWithStatus(iris.StatusBadRequest)
		helper.CreateErrorResponse(ctx, err).BadRequest().JSON()
		return
	}

	err = outlet.Delete(db.engine, id)
	if err != nil {
		ctx.StopWithStatus(iris.StatusInternalServerError)
		helper.CreateErrorResponse(ctx, err).InternalServer().JSON()
		return
	}

	helper.CreateResponse(ctx).Ok().SetMessage(fmt.Sprintf("%d deleted", id)).JSON()
}
