package outlet

import "xorm.io/xorm"

type Database struct {
	engine *xorm.Engine
}

// Init init database
func Init(DB *xorm.Engine) Database {
	return Database{engine: DB}
}
