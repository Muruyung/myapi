package config

import (
	"MyAPI/delivery/helper"
	"fmt"
	"strconv"
)

// DBENGINE get DB engine
func DBENGINE() string {
	return helper.GetEnv("DB_ENGINE", "postgres")
}

// DBCONFIG get DB Config
func DBCONFIG() string {
	host := helper.GetEnv("DB_HOST", "localhost")
	port := helper.GetEnv("DB_PORT", "5432")
	user := helper.GetEnv("DB_USER", "postgres")
	pass := helper.GetEnv("DB_PASS", "password")
	dbname := helper.GetEnv("DB_NAME", "postgres")
	ssl := helper.GetEnv("DB_SSL", "disable")

	return fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		host,
		port,
		user,
		pass,
		dbname,
		ssl,
	)
}

// JWTSECRETKEY get secret key for jwt
func JWTSECRETKEY() string {
	return helper.GetEnv("JWT_SECRET_KEY", "secret")
}

// JWTEXPIRATIONHOUR get expire time for jwt
func JWTEXPIRATIONHOUR() int {
	val, _ := strconv.Atoi(helper.GetEnv("JWT_EXPIRATION_HOUR", "24"))
	return val
}
