package config

import (
	"fmt"

	"xorm.io/xorm"
)

func DBase() *xorm.Engine {
	db, err := xorm.NewEngine(DBENGINE(), DBCONFIG())

	if err != nil {
		fmt.Println(err)
	}
	return db
}
