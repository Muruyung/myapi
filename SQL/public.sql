/*
 Navicat Premium Data Transfer

 Source Server         : Local PG
 Source Server Type    : PostgreSQL
 Source Server Version : 130007
 Source Host           : localhost:5432
 Source Catalog        : my-api
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130007
 File Encoding         : 65001

 Date: 14/07/2022 11:27:59
*/


-- ----------------------------
-- Table structure for merchants
-- ----------------------------
DROP TABLE IF EXISTS "public"."merchants";
CREATE TABLE "public"."merchants" (
  "id" int8 NOT NULL,
  "user_id" int8 NOT NULL,
  "merchant_name" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "created_by" int8 NOT NULL,
  "updated_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_by" int8 NOT NULL
)
;

-- ----------------------------
-- Records of merchants
-- ----------------------------
INSERT INTO "public"."merchants" VALUES (1657770969499, 1657770892811, 'merchant 1', '2022-07-14 10:56:09', 1657770892811, '2022-07-14 10:56:09', 1657770892811);
INSERT INTO "public"."merchants" VALUES (1657770993253, 1657770900425, 'merchant 2', '2022-07-14 10:56:33', 1657770900425, '2022-07-14 10:56:33', 1657770900425);

-- ----------------------------
-- Table structure for outlets
-- ----------------------------
DROP TABLE IF EXISTS "public"."outlets";
CREATE TABLE "public"."outlets" (
  "id" int8 NOT NULL,
  "merchant_id" int8 NOT NULL,
  "outlet_name" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "created_by" int8 NOT NULL,
  "updated_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_by" int8 NOT NULL
)
;

-- ----------------------------
-- Records of outlets
-- ----------------------------
INSERT INTO "public"."outlets" VALUES (1657772175286, 1657770969499, 'Outlet 1', '2022-07-14 11:16:15', 1657770892811, '2022-07-14 11:16:15', 1657770892811);
INSERT INTO "public"."outlets" VALUES (1657772179768, 1657770969499, 'Outlet 2', '2022-07-14 11:16:19', 1657770892811, '2022-07-14 11:16:19', 1657770892811);
INSERT INTO "public"."outlets" VALUES (1657772122747, 1657770993253, 'Outlet 1', '2022-07-14 11:15:22', 1657770900425, '2022-07-14 11:15:22', 1657770900425);

-- ----------------------------
-- Table structure for tokens
-- ----------------------------
DROP TABLE IF EXISTS "public"."tokens";
CREATE TABLE "public"."tokens" (
  "id" int8 NOT NULL,
  "id_user" int8 NOT NULL,
  "token" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of tokens
-- ----------------------------
INSERT INTO "public"."tokens" VALUES (1657764933548, 1657764933546, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI6ImNvYmFfdXNlcm5hbWVfZ2FuIiwiZXhwIjoxNjU3ODU2Njc2LCJpc3MiOiJBdXRoU2VydmljZSJ9.YfqveTywvIE-e1qJnUVMxe4U4GG6v5p4nOjninwh6zo');
INSERT INTO "public"."tokens" VALUES (1657770900429, 1657770900425, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI6ImFkbWluMiIsImV4cCI6MTY1Nzg1NzM4MywiaXNzIjoiQXV0aFNlcnZpY2UifQ.Kek6649zmGBZSqs43MBWGKTcl_CQeHyI_9OIIvW9o9U');
INSERT INTO "public"."tokens" VALUES (1657770892814, 1657770892811, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI6ImFkbWluMSIsImV4cCI6MTY1Nzg1ODU1MCwiaXNzIjoiQXV0aFNlcnZpY2UifQ.ahkPY5Conh6l24LHTKxAXdLY4l2sWW33Q72w8fDiLx0');

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS "public"."transactions";
CREATE TABLE "public"."transactions" (
  "id" int8 NOT NULL,
  "merchant_id" int8 NOT NULL,
  "outlet_id" int8 NOT NULL,
  "bill_total" float8 NOT NULL,
  "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "created_by" int8 NOT NULL,
  "updated_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_by" int8 NOT NULL
)
;

-- ----------------------------
-- Records of transactions
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int8 NOT NULL,
  "name" varchar(45) COLLATE "pg_catalog"."default",
  "user_name" varchar(45) COLLATE "pg_catalog"."default",
  "password" varchar(225) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "created_by" int8 NOT NULL,
  "updated_at" timestamp(6),
  "updated_by" int8 NOT NULL
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (1657770892811, 'Admin 1', 'admin1', '$2a$14$Ix6ZHnGHYRSkf9glRWcLr.BThNYkt599Lj/rAQz0jFB1g/A5TdICi', '2022-07-14 10:54:52', 1657770892811, '2022-07-14 10:54:52', 1657770892811);
INSERT INTO "public"."users" VALUES (1657770900425, 'Admin 2', 'admin2', '$2a$14$ar7Qs2QxKdQB5aHQOgrr1enntt1cI11C3tno5EFCKrkZ141RcM1ua', '2022-07-14 10:55:00', 1657770900425, '2022-07-14 10:55:00', 1657770900425);

-- ----------------------------
-- Primary Key structure for table merchants
-- ----------------------------
ALTER TABLE "public"."merchants" ADD CONSTRAINT "Merchants_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table outlets
-- ----------------------------
ALTER TABLE "public"."outlets" ADD CONSTRAINT "Outlets_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table transactions
-- ----------------------------
ALTER TABLE "public"."transactions" ADD CONSTRAINT "Transactions_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("id");
